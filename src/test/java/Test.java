import classes.Auto;
import classes.Control;
import org.bson.Document;

public class Test {

    @org.junit.Test
    public void addAutos(){
        Auto b = new Auto("VW","Käfer","350");
        Document d = new Document()
                .append("Marke",b.getMarke())
                .append("Model",b.getModel())
                .append("PS",b.getPS());

        Control.addAuto(d);
    }

    @org.junit.Test
    public void printAutos(){
        Control.getAutos();
    }
}
