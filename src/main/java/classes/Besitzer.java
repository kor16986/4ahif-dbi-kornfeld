package classes;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Besitzer {
    String Vorname;
    String Nachname;
    List<Auto> Autos = new List<Auto>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<Auto> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(Auto auto) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Auto> c) {
            return false;
        }

        @Override
        public boolean addAll(int index, Collection<? extends Auto> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public Auto get(int index) {
            return null;
        }

        @Override
        public Auto set(int index, Auto element) {
            return null;
        }

        @Override
        public void add(int index, Auto element) {

        }

        @Override
        public Auto remove(int index) {
            return null;
        }

        @Override
        public int indexOf(Object o) {
            return 0;
        }

        @Override
        public int lastIndexOf(Object o) {
            return 0;
        }

        @Override
        public ListIterator<Auto> listIterator() {
            return null;
        }

        @Override
        public ListIterator<Auto> listIterator(int index) {
            return null;
        }

        @Override
        public List<Auto> subList(int fromIndex, int toIndex) {
            return null;
        }
    };

    public Besitzer(String vorname, String nachname, Auto autos) {
        Vorname = vorname;
        Nachname = nachname;
        Autos.add(autos);
    }

    public String getVorname() {
        return Vorname;
    }

    public void setVorname(String vorname) {
        Vorname = vorname;
    }

    public String getNachname() {
        return Nachname;
    }

    public void setNachname(String nachname) {
        Nachname = nachname;
    }

    public List<Auto> getAutos() {
        return Autos;
    }

    public void setAutos(List<Auto> autos) {
        Autos = autos;
    }
}
