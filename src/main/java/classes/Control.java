package classes;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.*;
import org.bson.Document;

import java.util.stream.StreamSupport;

public class Control {

    public static void addAuto(Document d){
        MongoClientURI mongoConnectionString = new MongoClientURI("mongodb://localhost:27017");
        MongoClient mongoClient = new MongoClient(mongoConnectionString);

        MongoDatabase database = mongoClient.getDatabase("Autobesitzer");
        ListCollectionsIterable<Document> collections = database.listCollections();

        MongoCollection<Document> collection = database.getCollection("Auto");

        collection.insertOne(d);


        mongoClient.close();
    }

    public static void getAutos(){
        MongoClientURI mongoConnectionString =
                new MongoClientURI("mongodb://localhost:27017");
        MongoClient mongoClient = new MongoClient(mongoConnectionString);

        MongoIterable<String> databaseNames = mongoClient.listDatabaseNames();

        MongoDatabase database = mongoClient.getDatabase("Autobesitzer");
        ListCollectionsIterable<Document> collections = database.listCollections();
        MongoCollection<Document> collection = database.getCollection("Auto");

        FindIterable<Document> documents = collection.find();
        StreamSupport
                .stream(documents.spliterator(), false)
                .forEach(System.out::println);

        mongoClient.close();
    }
}
