package classes;

public class Auto {
    String Marke;
    String Model;
    String PS;

    public Auto(String marke, String model, String PS) {
        Marke = marke;
        Model = model;
        this.PS = PS;
    }

    public String getMarke() {
        return Marke;
    }

    public void setMarke(String marke) {
        Marke = marke;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getPS() {
        return PS;
    }

    public void setPS(String PS) {
        this.PS = PS;
    }
}
