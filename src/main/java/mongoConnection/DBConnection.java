package mongoConnection;
import classes.Auto;
import classes.Besitzer;
import com.mongodb.*;
import com.mongodb.client.ListCollectionsIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.CreateCollectionOptions;
import com.mongodb.client.model.CreateViewOptions;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;

import java.util.List;
import java.util.Scanner;

public class DBConnection {

    public static void main(String[] args){

        MongoClientURI mongoConnectionString = new MongoClientURI("localhost:27017");
        MongoClient mongoClient = new MongoClient(mongoConnectionString);
        MongoIterable<String> databaseNames = mongoClient.listDatabaseNames();

        MongoDatabase database = mongoClient.getDatabase("spg-mongo");
        ListCollectionsIterable<Document> collections = database.listCollections();
        MongoCollection<Document> collection = database.getCollection("spg-mongo");

        mongoClient.close();
    }



}
